function initGoogleMap(name, options) {
  var elem = $(name);

  if(elem.length > 0) {
    var gMap = new google.maps.Map(elem[0], options);
    
    return gMap;    
  }
}

function placeGoogleMarker(coords, map, title) {
  var marker = new google.maps.Marker({ 
    position: coords,
    map: map,
    title: title
  });
}

function calcRoute(map, start, end) {
  var directionsDisplay = new google.maps.DirectionsRenderer();
  var directionsService = new google.maps.DirectionsService();

  directionsDisplay.setMap(map);

  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
  };
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}