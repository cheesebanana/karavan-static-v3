$(document).ready(function() {
  initBalloonTabs('#main-page-events');
});

function initBalloonTabs(name) {
  var elem = $(name);

  if(elem.length > 0) {
    elem.find('a:first').tab('show');
    // console.log(elem.find('a:first'));

    elem.find('a').click(function(e) {
      e.preventDefault();
      $(this).tab('show');
    });
  }
}

$(document).ready(function(){
  $('.fotorama').fotorama({
  	 width: '100%',
  	 maxheight: 595,
  	 margin: 0,
  	 autoplay: 10000,
  	 transitionDuration: 999,
  	 loop: true
  });
});
$(document).ready(function(){
  $('.fotorama_inside').fotorama({
		width: '100%',
		margin: 0,
		autoplay: 10000,
		transitionDuration: 999,
		nav: 'none',
		arrows: true,
		loop: true
  });
});
function initGoogleMap(name, options) {
  var elem = $(name);

  if(elem.length > 0) {
    var gMap = new google.maps.Map(elem[0], options);
    
    return gMap;    
  }
}

function placeGoogleMarker(coords, map, title) {
  var marker = new google.maps.Marker({ 
    position: coords,
    map: map,
    title: title
  });
}

// function calcRoute(map, start, end) {
function calcRoute(map, directionsDisplay, start, end) {
  if(!directionsDisplay) {
    var directionsDisplay = new google.maps.DirectionsRenderer();
  }
  var directionsService = new google.maps.DirectionsService();

  directionsDisplay.setMap(map);

  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
  };
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

function initGoogleAutocompleteMap(map, inputOrigin, destination) {
  var autocomplete = new google.maps.places.Autocomplete(inputOrigin);
  autocomplete.bindTo('bounds', map);
  var directionsDisplay = new google.maps.DirectionsRenderer();

  autocomplete.bindTo('bounds', map);

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      return;
    }

    var coords = [place.geometry.location.lat(), place.geometry.location.lng()];

    calcRoute(
      map,
      directionsDisplay,
      place.geometry.location,
      destination
    );
  });
}
$(document).ready(function(){

  $('#mobile_menu_btn').on("click", function() { 
    $('#main_menu').slideToggle(300); 
    // $('#h_block').slideToggle();    
  })  
  $('#mobile_menu_btn_close').on("click", function() { 
    $('#main_menu').slideToggle(300);  
    // $('#h_block').slideToggle();    
  })                                  
});

$(document).ready(function(){

	$(function () {
	    $("#nav").tinyNav();
	    $('.tinynav').addClass('select_menu');

	    $('.select_menu').select2({
		    minimumResultsForSearch: -1,
		    dropdownCssClass: "select_city_drop",
		    readonly: true
		  });

	    $(".select2-focusser").remove();
	 });



});
var i = 0;

$(document).ready(function(){
  $('.select2').select2();
  
  $('#select_city').select2({
    minimumResultsForSearch: -1,
    dropdownCssClass: "select_city_drop",
    readonly: true
  });

  $(".select2-focusser").remove();
  

  $('#select_city').on("select2-open", function() { 

    var val = $(this).select2("val");
    console.log(val);
    $(".select2-result-label").each(function() {
  //     $(this).parent().removeClass("select2-result-unselectable")
      if ($(this).text() == val) {
        $(this).parent().addClass("select2-result-unselectable");
      };
    });
  })                                  
});