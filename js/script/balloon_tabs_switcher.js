$(document).ready(function() {
  initBalloonTabs('#main-page-events');
});

function initBalloonTabs(name) {
  var elem = $(name);

  if(elem.length > 0) {
    elem.find('a:first').tab('show');
    // console.log(elem.find('a:first'));

    elem.find('a').click(function(e) {
      e.preventDefault();
      $(this).tab('show');
    });
  }
}
