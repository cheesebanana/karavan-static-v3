$(document).ready(function(){
  $('.select2').select2();
  
  $('#select_city').select2({
    minimumResultsForSearch: -1,
    dropdownCssClass: "select_city_drop",
    readonly: true
  });

  $(".select2-focusser").remove();
  

  $('#select_city').on("select2-open", function() { 

    var val = $(this).select2("val");
    console.log(val);
    $(".select2-result-label").each(function() {
  //     $(this).parent().removeClass("select2-result-unselectable")
      if ($(this).text() == val) {
        $(this).parent().addClass("select2-result-unselectable");
      };
    });
  })                                  
});